package org.example;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Student> students = new ArrayList<>();

        // Nhập danh sách học sinh
        System.out.println("Nhập thông tin cho các học sinh:");
        int numStudents;
        do {
            System.out.print("Nhập số lượng học sinh: ");
            numStudents = scanner.nextInt();
        } while (numStudents <= 0);

        for (int i = 0; i < numStudents; i++) {
            System.out.println("Nhập thông tin cho học sinh thứ " + (i + 1) + ":");
            System.out.print("ID: ");
            int id = scanner.nextInt();
            scanner.nextLine();
            System.out.print("Name: ");
            String name = scanner.nextLine();
            System.out.print("Age: ");
            int age = scanner.nextInt();
            scanner.nextLine();
            System.out.print("Address: ");
            String address = scanner.nextLine();
            System.out.print("Math score: ");
            double math = scanner.nextDouble();
            System.out.print("Physics score: ");
            double physics = scanner.nextDouble();
            System.out.print("Chemistry score: ");
            double chemistry = scanner.nextDouble();
            students.add(new Student(id, name, age, address, math, physics, chemistry));
        }
        Collections.sort(students, new Comparator<Student>() {
            @Override
            public int compare(Student s1, Student s2) {
                double average1 = s1.getAverage();
                double average2 = s2.getAverage();
                return Double.compare(average1, average2);
            }
        });
        System.out.println("Danh sách sinh viên theo điểm trung bình từ cao đến thấp:");
        for (Student student : students) {
            student.display();
            System.out.println();
        }

        Student maxMath = students.stream().max((a, b) -> Double.compare(a.getMath(), b.getMath())).get();
        System.out.println("Danh sách sinh viên theo điểm toán cao nhất:");
        maxMath.display();

        System.out.println("Danh sách sinh viên theo tuổi lớn hơn 23:");
        for (Student student : students) {
            if (student.getAge() > 23) {
                student.display();
                System.out.println();
            } else {
                System.out.println("Không có sinh viên nào lớn hơn 23 tuổi");
            }

        }

        System.out.println("Danh sách sinh viên có tên Hoàng:");
        for (Student student : students) {
            if (student.getName().startsWith("hoang")) {
                student.display();
                System.out.println();
            } else {
                System.out.println("Không có sinh viên nào họ hoàng");
            }

        }

        System.out.println("Danh sách sinh viên có địa chỉ Hà Nội:");
        for (Student student : students) {
            if (student.getAddress().equals("hn")) {
                student.display();
                System.out.println();
            } else {
                System.out.println("Không có sinh viên nào họ hoàng");
            }

        }
    }


}