package org.example;

public class Student extends Person{
    private double math;
    private double physics;
    private double chemistry;
    private double average;


    public Student() {
    }

    public Student(int id, String name, int age, String address, double math, double physics, double chemistry) {
        super(id, name, age, address);
        this.math = math;
        this.physics = physics;
        this.chemistry = chemistry;
    }

    public double getAverage(){
        return average = (math+physics+chemistry)/3;
    }
    public double getMath(){
        return math;
    }

    public void display(){
        super.display();
        System.out.println("Điểm toán: "+ math);
        System.out.println("Điểm lý: "+ physics);
        System.out.println("Điểm hóa: "+ chemistry);
        System.out.println("Điểm trung bình: "+ getAverage());
    }

}
